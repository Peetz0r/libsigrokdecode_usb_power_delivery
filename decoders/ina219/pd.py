##
## This file is part of the libsigrokdecode project.
##
## Copyright (C) 2018 Peter Hazenberg <sigrok@haas-en-berg.nl>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
##

import sigrokdecode as srd

class Decoder(srd.Decoder):
    api_version = 3
    id = 'ina219'
    name = 'INA219'
    longname = 'TI INA219'
    desc = 'Voltage/Current monitor'
    license = 'mit'
    inputs = ['i2c']
    outputs = ['ina219']
    annotations = (
        ('Voltage', 'Voltage in Volt'),
        ('Current', 'Current in Ampere'),
        ('Power'  , 'Power in Watts'),
        ('Changes', 'Large changes in voltage or current'),
        ('Setup'  , 'Configuration, calibration, etc'),
    )
    annotation_rows = (
        ('Measurements', 'Measurements', (0, 1, 2)),
        ('Changes', 'Changes', (3, )),
        ('Setup', 'Configuration, calibration, etc', (4, )),
    )
    options = (
        {'id': 'shunt_resistor',   'desc': 'Value of the shunt resistor in Ω', 'default': '0.1'},
        {'id': 'voltage_treshold', 'desc': 'Voltage change treshold (set to 0 to disable)', 'default': '0.5'},
        {'id': 'current_treshold', 'desc': 'Current change treshold (set to 0 to disable)', 'default': '0.5'},
        {'id': 'power_treshold',   'desc': 'Current change treshold (set to 0 to disable)', 'default': '0.5'},
    )

    registers = {
        0: 'CONF',
        1: 'V_SHUNT',
        2: 'V_BUS',
        3: 'POWER',
        4: 'CURRENT',
        5: 'CAL'
    }

    bus_voltage_ranges = {
        0: 16,
        1: 32, # default
    }

    gain_levels = {
        0: 1,   # ±  40 mV
        1: 1/2, # ±  80 mV
        2: 1/4, # ± 160 mV
        3: 1/8, # ± 320 mV
    }

    adc_settings = { # some duplicates because some bits are "Don't care"
        0b0000:   '9 bit', #    84 µs
        0b0100:   '9 bit', #    84 µs, don't-care duplicate of above
        0b0001:  '10 bit', #   148 µs
        0b0101:  '10 bit', #   148 µs, don't-care duplicate of above
        0b0010:  '11 bit', #   276 µs
        0b0110:  '11 bit', #   276 µs, don't-care duplicate of above
        0b0011:  '12 bit', #   532 µs, default
        0b0111:  '12 bit', #   532 µs, default, don't-care duplicate of above
        0b1000:  '12 bit', #   532 µs, regular duplicate of above
        0b1001:   '2 avg', #  1.06 ms
        0b1010:   '4 avg', #  2.13 ms
        0b1011:   '8 avg', #  4.26 ms
        0b1100:  '16 avg', #  8.51 ms
        0b1101:  '32 avg', # 17.02 ms
        0b1110:  '64 avg', # 34.05 ms
        0b1111: '128 avg', # 68.10 ms
    }

    modes = {
        0: 'Power-down',
        1: 'Shunt voltage, triggered',
        2: 'Bus voltage, triggered',
        3: 'Shunt and bus voltage, triggered',
        4: 'ADC off (disabled)',
        5: 'Shunt voltage, continuous',
        6: 'Bus voltage, continuous',
        7: 'Shunt and bus voltage, continuous', # default
    }

    def __init__(self):
        self.reset()

    def reset(self):
        self.state = None
        self.last_voltage = None
        self.last_current = None
        self.cal = None
        self.data = []
        self.bus_voltage_range = self.bus_voltage_ranges.get(1)
        self.gain = 1/8
        self.shunt_adc_setting = '12 bit'
        self.bus_adc_setting = '12 bit'
        self.mode = 0

    def start(self):
        self.out_ann = self.register(srd.OUTPUT_ANN)

    def putx(self, data):
        self.put(self.ss, self.es, self.out_ann, data)

    def decode(self, ss, es, data):
        cmd, databyte = data

        if cmd == 'START':
            self.state = None
            self.data = []
            self.ss = ss

        elif cmd.startswith('DATA'):
            if self.state == None:
                self.state = self.registers.get(databyte, databyte)
            else:
                self.data.append(databyte)

        elif cmd == 'STOP':
            self.es = es
            print('State: %s\tData: %s' % (self.state, repr(self.data)))

            if self.state == 'V_SHUNT':
                mask = {
                    1/8: 0b1111111111111111,
                    1/4: 0b1011111111111111,
                    1/2: 0b1001111111111111,
                    1/1: 0b1000111111111111,
                }.get(self.gain)

                current = (int.from_bytes(self.data, byteorder='big', signed=True) & mask) * 0.00001/0.1

                self.putx([0, ['%3.2f A' % current]])
                if self.options['current_treshold'] != 'None':
                    if self.last_current is None:
                        self.putx([3, ['%3.2f A' % (current)]])
                        self.last_current = current
                    if abs(current - self.last_current) > float(self.options['current_treshold']):
                        self.putx([3, ['%3.2f A -> %3.2f A' % (self.last_current, current)]])
                        self.last_current = current

            elif self.state == 'CURRENT' and self.cal is not None:
                current = int.from_bytes(self.data, byteorder='big', signed=True)/4096
                self.putx([0, ['Current %3.2f A' % current]])
                if self.options['current_treshold'] != 'None':
                    if self.last_current is None:
                        self.putx([3, ['%3.2f A' % (current)]])
                        self.last_current = current
                    if abs(current - self.last_current) > float(self.options['current_treshold']):
                        self.putx([3, ['%3.2f A -> %3.2f A' % (self.last_current, current)]])
                        self.last_current = current

            elif self.state == 'V_BUS':
                voltage = ((self.data[0] << 5) + (self.data[1] >> 3)) * 0.004
                self.putx([1, ['%3.2f V' % voltage]])
                if self.options['voltage_treshold'] != 'None':
                    if self.last_voltage is None:
                        self.putx([3, ['%3.2f V' % (voltage)]])
                        self.last_voltage = voltage
                    if abs(voltage - self.last_voltage) > float(self.options['voltage_treshold']):
                        self.putx([3, ['%3.2f V -> %3.2f V' % (self.last_voltage, voltage)]])
                        self.last_voltage = voltage

            elif self.state == 'CAL':
                self.cal = int.from_bytes(self.data, byteorder='big', signed=False) >> 1
                self.putx([4, ['CAL! %3.2f' % self.cal]])

            elif self.state == 'CONF':
                bits = int.from_bytes(self.data, byteorder='big', signed=False)
                reset = (bits >> 15) & 0b1 == 1

                if(reset):
                    self.putx([4, ['CONF: reset!']])
                    self.reset()

                self.bus_voltage_range = self.bus_voltage_ranges.get((bits >> 13) & 0b1)
                self.gain = self.gain_levels.get((bits >> 11) & 0b11)
                self.bus_adc_setting = self.adc_settings.get((bits >> 7) & 0b1111)
                self.shunt_adc_setting = self.adc_settings.get((bits >> 3) & 0b1111)

                self.mode = self.modes.get((bits >> 11) & 0b11)

                self.putx([4, ['CONF!\n - mode:\t%s\n - shunt adc:\t%s\n - bus adc:\t%s\n - gain:\t%s\n - range:\t%s\n - reset:\t%s' %
                    (self.mode, self.shunt_adc_setting, self.bus_adc_setting, self.gain, self.bus_voltage_range, reset)]])

            else:
                self.putx([4, ['unimplemented state %s\tData: %s' % (self.state, repr(self.data))]])
